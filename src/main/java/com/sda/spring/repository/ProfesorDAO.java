package com.sda.spring.repository;

import com.sda.spring.config.HibernateUtil;
import com.sda.spring.entities.Profesor;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.List;

@Repository
public class ProfesorDAO {
    HibernateUtil hibernateUtil;

    public ProfesorDAO(){
        hibernateUtil = new HibernateUtil();
    }

    public void insertProfessor(Profesor profesor){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        session.persist(profesor);
        transaction.commit();
        session.close();
    }

    public void deleteProfessor(String firstName){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        Query deleteProfessor = session.createNamedQuery("delete_professor_by_name");
        deleteProfessor.setParameter("firstName", firstName);
        deleteProfessor.executeUpdate();
        transaction.commit();
        session.close();
    }

    public List<Profesor> selectProfesor(String firstName){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        Query selectProfessor = session.createNamedQuery("find_professor_by_name");
        selectProfessor.setParameter("firstName", firstName);
        List<Profesor> professors = selectProfessor.getResultList();
        transaction.commit();
        session.close();
        return professors;
    }
}
