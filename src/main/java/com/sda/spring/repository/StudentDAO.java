package com.sda.spring.repository;

import com.sda.spring.config.HibernateUtil;
import com.sda.spring.entities.Student;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.LinkedList;
import java.util.List;

@Repository
public class StudentDAO {
    private HibernateUtil hibernateUtil;

    public StudentDAO() {
        hibernateUtil = new HibernateUtil();
    }

    public void insertStudent(Student student) {
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        session.persist(student);
        transaction.commit();
        session.close();
    }

    public void deleteStudent(String firstName) {
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        Query deleteQuery = session.createNamedQuery("delete_student_by_name");
        deleteQuery.setParameter("firstName", firstName);
        deleteQuery.executeUpdate();
        transaction.commit();
        session.close();
    }

    public List<Student> selectStudent(String firstName){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        Query selectStudent = session.createNamedQuery("find_student_by_name");
        selectStudent.setParameter("firstName", firstName);
        List<Student> students = selectStudent.getResultList();
        transaction.commit();
        session.close();
        return students;
    }

    public void updateStudent(Student student){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        Query updateStudent = session.createNamedQuery("update_student");
        updateStudent.setParameter("firstName", student.getFirstName()).setParameter("lastName", student.getLastName()).setParameter("group", student.getGroup());
        updateStudent.executeUpdate();
        transaction.commit();
        session.close();
    }

}
