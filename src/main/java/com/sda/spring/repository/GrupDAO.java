package com.sda.spring.repository;

import com.sda.spring.config.HibernateUtil;
import com.sda.spring.entities.Grup;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.List;

@Repository
public class GrupDAO {
    HibernateUtil hibernateUtil;

    public GrupDAO() {
        hibernateUtil = new HibernateUtil();
    }

        public void insertGroup(Grup grup){
            Session session = hibernateUtil.getSession();
            Transaction transaction = session.beginTransaction();
            session.persist(grup);
            transaction.commit();
            session.close();
        }

        public void deleteGrup(String name){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        Query deleteGrup = session.createNamedQuery("delete grup_by_name");
        deleteGrup.setParameter("name", name);
        deleteGrup.executeUpdate();
        transaction.commit();
        session.close();
        }

        public List<Grup> selectGrup(String name){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        Query selectGrup = session.createNamedQuery("find_class_by_id");
        selectGrup.setParameter("name", name);
        List<Grup> grups = selectGrup.getResultList();
        transaction.commit();
        session.close();
        return grups;
        }
    }

