package com.sda.spring.dto;

public class ProfessorDTO {
    private String firstName;
    private String lastName;
    private String materie;

    public ProfessorDTO(){

    }

    public ProfessorDTO(String firstName, String lastName, String materie) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.materie = materie;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMaterie() {
        return materie;
    }

    public void setMaterie(String materie) {
        this.materie = materie;
    }

    @Override
    public String toString() {
        return "ProfessorDTO{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", materie='" + materie + '\'' +
                '}';
    }
}
