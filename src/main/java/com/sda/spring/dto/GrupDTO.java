package com.sda.spring.dto;

public class GrupDTO {
    private String name;
    private int studentNum;

    public GrupDTO(String name, int studentNum) {
        this.name = name;
        this.studentNum = studentNum;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStudentNum() {
        return studentNum;
    }

    public void setStudentNum(int studentNum) {
        this.studentNum = studentNum;
    }

    @Override
    public String toString() {
        return "GrupDTO{" +
                "name='" + name + '\'' +
                ", studentNum=" + studentNum +
                '}';
    }
}
