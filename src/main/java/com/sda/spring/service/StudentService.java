package com.sda.spring.service;

import com.sda.spring.dto.StudentDTO;
import com.sda.spring.entities.Student;
import com.sda.spring.repository.StudentDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
public class StudentService {
    @Autowired
    private StudentDAO studentDAO;

    public void insertStudent(StudentDTO studentDTO) {
        Student student = new Student();
        student.setFirstName(studentDTO.getFirstName());
        student.setLastName(studentDTO.getLastName());
        student.setGroup(studentDTO.getGroup());
        studentDAO.insertStudent(student);
    }

    public void deleteStudent(String name){
        studentDAO.deleteStudent(name);
    }

    public List<StudentDTO> findStudent(String name){
        List<Student> students = studentDAO.selectStudent(name);
        List<StudentDTO> studentDTOS = new LinkedList<>();
        for(Student s: students){
            StudentDTO studentDTO = new StudentDTO();
            studentDTO.setFirstName(s.getFirstName());
            studentDTO.setLastName(s.getLastName());
            studentDTO.setGroup(s.getGroup());
            studentDTOS.add(studentDTO);
        }
        return studentDTOS;
    }

    public void updateStudent(String name){
       
    }
}
