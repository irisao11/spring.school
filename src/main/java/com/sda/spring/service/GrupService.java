package com.sda.spring.service;

import com.sda.spring.dto.GrupDTO;
import com.sda.spring.dto.StudentDTO;
import com.sda.spring.entities.Grup;
import com.sda.spring.entities.Student;
import com.sda.spring.repository.GrupDAO;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GrupService {
    @Autowired
    GrupDAO grupDAO;

    public void insertGrup(GrupDTO grupDTO){
        Grup grup = new Grup();
        grup.setName(grupDTO.getName());
        grup.setStudentNum(grupDTO.getStudentNum());
        grupDAO.insertGroup(grup);
    }

public void deleteGrup(String name){
        grupDAO.deleteGrup(name);
}
}
