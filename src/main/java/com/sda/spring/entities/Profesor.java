package com.sda.spring.entities;

import javax.persistence.*;

@NamedQueries({
        @NamedQuery(name = "delete_professor_by_name",
                query = "delete from Profesor p where p.firstName =:firstName"),
        @NamedQuery(name = "find_professor_by_name",
                query = "select p from Profesor p where p.firstName = :firstName")
                })

@Table(name="professors")
@Entity
public class Profesor {
   // private static final String PROFESSOR_SEQUENCE = "profesor_id_seq";
    //private static final String PROFESSOR_GENERATOR = "profesor_generator";

    @Id
    private int id;

    @Column(name = "prenume")
    private String firstName;

    @Column(name="nume")
    private String lastName;

    @Column(name="materie")
    private String materie;

    public Profesor() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMaterie() {
        return materie;
    }

    public void setMaterie(String materie) {
        this.materie = materie;
    }
}
