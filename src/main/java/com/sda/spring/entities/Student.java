package com.sda.spring.entities;

import javax.persistence.*;
import java.util.Objects;
@NamedQueries({
        @NamedQuery(name = "delete_student_by_name",
                query = "delete from Student s where s.firstName = :firstName"),
        @NamedQuery(name = "find_student_by_name",
                query = "select s from Student s where s.firstName = :firstName"),
        @NamedQuery(name = "update_student",
        query = "update Student s set s.firstName = :firstName, s.lastName = :lastName, s.group = :group")
})

@Entity
@Table(name = "students")
public class Student {

   // private static final String STUDENT_SEQUENCE = "student_id_seq";
    //private static final String STUDENT_GENERATOR = "student_generator";

    @Id
    private int id;

    @Column(name = "nume")
    private String lastName;

    @Column(name = "prenume")
    private String firstName;

    @Column(name = "clasa")
    private String group;

    public Student() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return id == student.id &&
                Objects.equals(lastName, student.lastName) &&
                Objects.equals(firstName, student.firstName) &&
                Objects.equals(group, student.group);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, lastName, firstName, group);
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", lastName='" + lastName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", group='" + group + '\'' +
                '}';
    }
}
