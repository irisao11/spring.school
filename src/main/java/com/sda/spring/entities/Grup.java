package com.sda.spring.entities;

import javax.persistence.*;

@NamedQueries({
        @NamedQuery(name="delete grup_by_name",
        query = "delete from Grup g where g.name = :name"),
        @NamedQuery(name="find_class_by_id",
        query = "select g from Grup g where g.name = :name")
})

@Entity
@Table(name= " clase")
public class Grup {

    @Id
    private int id;

    @Column(name = "nume")
    private String name;

    public int getStudentNum() {
        return studentNum;
    }

    public void setStudentNum(int studentNum) {
        this.studentNum = studentNum;
    }

    @Column(name="numar_studenti")
    private int studentNum;

    public Grup() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
